# Technical changes on transition to RI

This documents all known techical changes that will happen to the IAM-interfaces
when UiB transitions from SEBRA to RI. This is currently planned to take place [2022-04-02](https://www.uib.no/tilgang).

## SCIM

[SCIM](https://git.app.uib.no/it-bott-integrasjoner/iam-scim/-/blob/master/README.md) is the official REST API to obtain information about user accounts from the IGA.
Both SEBRA and RI implements this API but there are some differences that clients need
to handle.

Access to SCIM is obtained from [UiBs API-GW](https://api-uib.intark.uh-it.no/#!/apis/91a73d99-d9b2-452a-a73d-99d9b2e52a9a/detail).


Changes:

* The version number goes from 1.x to 2.0. This is reflected both in the path
  exposed over the API GW and returned by the `/version` endpoint.

* MQ Messages on changes to SCIM are now published by the *system-production-IGA-Tilgang* application in [BROM](https://brom-uib.intark.uh-it.no/application/system-production-iga-tilgang). Client applications need to subscribe to messages from this source and might retire the subscription to events from *system-production-IGA-SEBRA*. There are corresponding applications in the *system-test-\** namespace.

* The user IDs are UUID strings instead of integers (`uidNumber`). The previous ID is not exposed in the new SCIM v2.

* The `externalId` is now the UH-ID of account owner. This used to be the internal SEBRA ID of the owner.
  For primary accounts the account ID happens to be the same — that is `id` and `externalId` are equal.

* `filter='userName eq "XXXX"'` doesn't return results for unqualified
  usernames any more.  The unqualified form is supported for the `userName=XXXX` form, or one can
  use `filter='userName sw "XXXX@"` instead.

* `attributes=no:edu:scim:user` does not select all attributes within that schema. You have to be explicit
  about what attributes to return. For instance use `attributes=no:edu:scim:user:employeeNumber` if you want
  to select that attribute.

* `attributes=no:edu:scim:user.employeeNumber` selected the employeeNumber. In RI you have to use the standard
  SCIM attribute naming with ':' before the final segment as well.

* The `no:edu:scim:user.employeeNumber` is now returned zero-padded (same form the ID is returned by the DFØ API).
  For instance an employee number like "6099999" is now returned as "06099999".

* The type of `no:edu:scim:user.primaryOrgUnit.legacyStedkode` changes from number to string.

* The `Accept:` header is honored in HTTP requests. If provided it needs to
 include `application/scim+json` which is not the same as `application/json`.

New features:

* New attributes in the User objects are:

  - `title`
  - `profileUrl`
  - `preferredLanguage`
  - `roles`
  - `urn:ietf:params:scim:schemas:extension:enterprise:2.0:User.employeeNumber`
  - `urn:ietf:params:scim:schemas:extension:enterprise:2.0:User.manager`
  - `no:edu:scim:user.fsPersonNumber`
  - `no:edu:scim:user.studentNumber`
  - `no:edu:scim:user.gregPersonNumber`
  - `meta.location`

* RI implements the /Groups endpoint. This exposes the internal RI Portal Roles
and is not the same as the groups you find in LDAP.

* The `filter` expressions support much more of the SCIM-standard. The SEBRA implementation didn't have a
  real expression parser and it only allowed the `eq`-operator on a select few attributes. Examples of searches that now work:

  ```
  .../Users?filter=emails sw "per.ol"
  .../Users?filter=emails eq "per.olsen@uib.no"
  .../Users?filter=name.familyName eq "Olsen"
  .../Users?filter=name.givenName eq "Per" and name.familyName eq "Olsen"
  .../Users?filter=roles eq "iam:programstudent"
  ```

  In general everything that is documented at <https://ldapwiki.com/wiki/SCIM%20Filtering> now works.

Limitations:

* The `attributes=name.formatted` field doesn't work. Use `attributes=name` as workaround. #PT-453

* When person names change, then event messages doesn't indicate that `name.formatted` changed. Look for changes to `name.givenName` and/or `name.familyName` as workaround. #PT-602

* The `no:edu:scim:user.employeeType` is missing.

## SWS

The SWS services are all discontinued.
The needs that these fullfilled must be solved by using SCIM and other
master data APIs directly. Visit the [API-GW](https://api-uib.intark.uh-it.no/#!/) to obtain access to alternative APIs.

## Oracle

There will be an Oracle database made available with a subset of views provided from SEBRA today.
The following tables are maintained:

* apex_personer
* apex_f_lonnsdag
* apex_s_lonnsdag
* apex_studenter
* apex_telefon

## LDAP

Changed attributes:

* `mail` the address is now mixed case; Fornavn.Etternavn@uib.no instead of fornavn.etternavn@uib.no
* `eduPersonPrimaryOrgUnitDN` the DN format has changed to use symbolic name; OU=ADM-IT instead of OU=221000. The numeric code is still available in the same format as before from the `departmentNumber` attribute of users and inside the OrgUnit object itself as the `norEduOrgUnitUniqueIdentifier` attribute.
* `loginShell` is forced to have the value `/bin/bash` for all users.
* `gcos` names with 'æ', 'ø', 'å' get a different ASCII-fication ('ae', 'oe', 'aa').

Removed attributes:

* `apple-user-homeurl`

These attributes are added to the user objects:

* `employeeNumber` matches the employeeNumber from DFØ
* `l`: postal place name of the office address (corresponds to `postalCode`).
* `postalCode`: postal code of the office address

For students we didn't fill in the `eduPersonPrimaryOrgUnitDN` and `ou` attributes. RI does that now.

## AD

Changed user attributes:

* `DN`: Now follows the pattern `CN=<username>,OU=<usertype>,OU=people,OU=UH-IAM,DC=uib,DC=no`

* `department`: Changed from long form to the symbolic name; for instance “Arkitektur og skysatsning» to «ADM-IT-ARK»

* `description`: Changed from duplicate of legacy ‘stedkode’ found in `departmentNumber` to the title that corresponds to `department`; for instance “229911” to “Arkitektur og skysatsning”

These attributes are added to the user objects:

* `employeeNumber` is filled with the student number from FS

* `employeeID` is filled with the employee number from DFØ/SAP

* `manager`: DN of the primary manager for this person.  Short term guests will have their sponsor registered as manager.

* `directReports`: DN of all those that have this user as their manager.

* `physicalDeliveryOfficeName` is yet another copy of the symbolic unit name.

Differences from LDAP:

* `DN` path includes `OU=UH-IAM` as well as `OU=<usertype>` in AD.

* `mail` is forced to lowercase; while LDAP uses mixed casing.

* `employeeNumber` is what the name implies in LDAP, while it is the *student* number in AD 🙈.
