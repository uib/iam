# Architecture of current IAM at UiB

This is a description of the stuff that make up IAM at UiB as of spring 2019.

The architecture diagrams in this article uses the
[ArchiMate](https://en.wikipedia.org/wiki/ArchiMate) notation.  The various
types of boxes and connectors between them have specific meaning, but we'll try
to explain the main points with text which should make it possible to follow
along without prevous knowledge of ArchiMate. The diagrams are generated using
the [Archi tool](https://www.archimatetool.com) which can also be used to
investigate the full models.

## IGA

We start out with this simple diagram which illustrates that the current IGA
service at UiB is compomised of 3 different applications that all run on top of
the same data store. The main sources of data about persons originate in FS
(for student) and Paga (for university employees).

![Archimate diagram showing basic IGA structure](https://git.app.uib.no/uib/iam/raw/master/arch/exp/IGA-bare.svg)

If we add the roles and the major ways data from the IGA is exposed to the world we end up wih this picture.

![Archimate diagram showing IGA roles](https://git.app.uib.no/uib/iam/raw/master/arch/exp/IGA-decorated.svg)

The identified roles within the IGA are:

* *SEBRA-admin* which can basically do anything which is given to the MIT-team at IT.
* *Godkjenner* which can register persons, create new user accounts, and set expiry date for accounts. A few persons within each department are designated for this role.
* *Super-godkjenner* which can administer who get the role *Godkjenner*. This role also get to maintain master and reference data that our IGA keeps for lack of a better source.
* *Student-godkjenner* which can manage expiry for students. This role goes to the admins of our FS.
* *Operatør/Administrator* which can do the stuff that user support feel they need to do.
* *Leser* can view the same stuff as *Operatør/Administrator* but can not make any changes.

At the lower end of the diagram we have illustrated the way data is exposed
from the IGA. SWS is the web API provided by SEBRA.  Data on users,
organisation and groups are also exposed through the catalog services LDAP, AD and Azure AD.
Data flows in mysterious ways, but mainly from LDAP to AD to Azure AD.

## Student Affairs

Next up is illustrating the interactions between IGA and the Division of Student Affairs (SA) where the main information system is FS.

![Archimate diagram showing student affairs](https://git.app.uib.no/uib/iam/raw/master/arch/exp/SA.svg)

The FS service provide services to *students* and administrive staff. Access
within FS is controlled by the *FS admins* but we use the *TAS* application for
the workflow involved in requesting access though the *Godkjenner* role.

The main route for information to flow between SEBRA and FS happens by
integration scripts in the *SEBRA cron* system that connects to both databases
and shuffle information between them. New students and updates are fetched from
FS and written to SEBRA. This triggeres generation of user accounts in SEBRA
and information on the username and e-mail addresses are written back to FS.
Teachers knows to SEBRA are written into the FS database as Fagpersoner.

SEBRA obtain card information from LENEL and will then generate the *Kortnr*
file, which is read by FS periodically. Just an example of another integration style.

Most recent is that SEBRA cron fetches access groups for each study from the
FS-rest application (REST API).  These are groups are written directly to LDAP
and allow the FS admins to control who belongs to each group.

## HR

Next up is illustrating the interactions between IGA and HR where the main information system is Paga.

![Archimate diagram showing HR](https://git.app.uib.no/uib/iam/raw/master/arch/exp/HR.svg)

Information flows between Paga and SEBRA in the form of CSV-files.  Paga
generates daily a file with one row for each active employment. A single person
can have multiple employments. This file is read by SEBRA and new person
objects and accounts are generated accordningly. In return SEBRA daily generates
a file with usernames, e-mail and phone numbers.

## Access cards

Identification cards that works as keys to the University buildings are producted and controlled by the LENEL system.

![Archimate diagram showing production of access cards](https://git.app.uib.no/uib/iam/raw/master/arch/exp/Kort.svg)

## Storage

![Archimate diagram showing interactions with the storage system](https://git.app.uib.no/uib/iam/raw/master/arch/exp/Lagring.svg)

## E-mail

![Archimate diagram showing interactions with the E-mail system](https://git.app.uib.no/uib/iam/raw/master/arch/exp/Mail.svg)

## Microsoft

The Microsoft ecosystem is important to us. The next diagram
illustrates how identity and access data flows in that world:

![Archimate diagram showing our Microsoft suite IAM](https://git.app.uib.no/uib/iam/raw/master/arch/exp/MS.svg)

The *sebra.vbs* is invoked from SEBRA when new user accounts are created
and placed in the AD tree.  This creates a new *user* object in AD with the
provided user name and sets the password.  This is enough for uses to start
using the account for basic stuff.  At the same time SEBRA will populate LDAP
with the corresponding full user entry.

The *ldap2ad* application will wake up periodically and fill the
user object in AD with all the other attributes found in LDAP.  The same
service will also notice any subsequent updates to LDAP and update AD to match.
User objects in AD can have extra attributes that are not in LDAP and which are
not written by this syncer.

When a user needs to be blocked Sebra will again invole *sebra.vbs* which
will keep the object around but move it to some disabled location.  The
corresponding user will be deleted from LDAP. The reason the objects isn't
deleted from AD is to keep its UUID around in case the account need to be
reactivated.

SEBRA also access AD directly to create and maintain its groups. All groups
that SEBRA puts in LDAP also gets written to AD.

There are many other groups and users in AD that are maintained directly by the
*Windows admins* manually or using various Power shell scripts.

## LDAP

LDAP is a central service used for distributing identity and access information.
The purpose of this diagram is to show which clients use LDAP.

![Archimate diagram showing usage of the ldap service](https://git.app.uib.no/uib/iam/raw/master/arch/exp/LDAP.svg)

There are two sources of the information stored in our LDAP directory. SEBRA
will create, update and delete users, groups and organisations. We also have a
human LDAP admin that will add their own users and groups.

## The web site

The IGA has aquired specific roles in providing data for UiB's main web site. This diagram illustrates the flow of data in that direction.

![Archimate diagram showing how IGA relates to our web site](https://git.app.uib.no/uib/iam/raw/master/arch/exp/EW.svg)
