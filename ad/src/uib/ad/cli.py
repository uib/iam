import click

@click.group()
def main():
    """Extract informaion from AD at UiB"""

def ldap(path):
    import subprocess
    subprocess.call(['curl', '-K', 'curl-config', 'ldap://fenris.uib.no/' + path])

@main.command()
@click.argument('dn')
def show(dn):
    ldap(dn + '??base')

@main.command()
@click.argument('dn')
def list(dn):
    if ',' not in dn:
        dn = ','.join(['ou=' + p for p in reversed(dn.split('/'))]) + ',dc=uib,dc=no'
    ldap(dn + '?dn?one')

@main.command()
@click.argument('username')
def user(username):
    ldap(f"ou=uib,ou=sebra,dc=uib,dc=no?dn?sub?(cn={username})")
    ldap(f"ou=kurs,ou=sebra,dc=uib,dc=no?dn?sub?(cn={username})")
