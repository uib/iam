Install:

    poetry install

Save your user config required to connect to AD:

    echo '-su "<myusername>@uib.no:<mypassword>"' >curl-config
    poetry run ad

Usage:

    ad list sebra
    ad list sebra/uib
    ad list uh-iam
    ad list uh-iam/people
    ad list uh-iam/groups
    ad list spesialgrupper/ldap
    ad list 'Exchange Resources/Rooms'
    ad user gaa041
    ad show CN=gaa041,OU=it,OU=adm,OU=uib,OU=Sebra,DC=uib,DC=no

Unfortunately, it's not possible to 'list' the content of the DC=uib,DC=no
node. See example above of some of the names at the root.
